// initial third party
const express = require("express");
const app = express();
const expressLayouts = require("express-ejs-layouts");
const PORT = process.env.PORT || 3000; // PORT

app.set("view engine", "ejs"); // npm install --save ejs
app.use(expressLayouts); // validate express ejs layout
app.use(express.static(__dirname + "/views")); // validate static file (CSS, js, dll)

// endpoint ========================================
app.get("/", (req, res) => {
    var locals = {
        title: "Dashboard Binar Rentel",
        layout: "layouts/main-layouts",
    };
    res.render("index", locals);
});

app.get("/add", (req, res) => {
    var locals = {
        title: "Add New Car",
        layout: "layouts/main-layouts",
    };
    res.render("add_car", locals);
});

app.use("/update", (req, res) => {
    var locals = {
        title: "Update Car",
        layout: "layouts/main-layouts",
    };
    res.render("update_car", locals);
});

app.use((req, res) => {
    var locals = {
        title: "Page Not Found!",
        layout: "layouts/main-layouts",
    };
    res.status(404).render("404", locals);
});

// PORT listen ========================================
app.listen(PORT, () => {
    console.log(`http://localhost:${PORT}`);
});
